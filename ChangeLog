2022-02-02 Mike Gabriel

        * Release 2022.01 (HEAD -> main, tag: 2022.01)

2021-12-01 Marius Gripsgard

        * Merge branch 'personal/peat-psuwit/merge-xenial-2021Nov' into
          'main' (810c7d1)

2021-11-29 Ratchanan Srirattanamet

        * Merge remote-tracking branch 'gitlab_ubports/ubports/xenial' into
          personal/peat-psuwit/merge-xenial-2021Nov (097638b)

2021-08-12 Florian Leeber

        * Merge branch 'personal/usb/suspend-delay-3' into 'ubports/xenial'
          (69e3ead)

2021-08-11 Dalton Durst

        * default_state_machine: 4 second delay before suspend (7ce336d)

2021-07-20 Dalton Durst

        * Merge branch 'personal/peat-psuwit/rename-to-lomiri' into 'main'
          (481457c)

2021-06-30 Dalton Durst

        * Merge branch 'personal/peat-psuwit/fix-gh-ut-1747' into
          'ubports/xenial' (a566d2c)

2021-06-30 Ratchanan Srirattanamet

        * default_daemon_config: actually return created UbuntuLightSensor
          (ee41141)
          Fixes:
          https://github.com/ubports/ubuntu-touch/issues/1747

2021-06-04 Ratchanan Srirattanamet

        * CMakeLists: bump version to match d/changelog (dbf8e0c)
        * d/{control,rules}: accommodate cmake-extras migration & no
          platform-api (650abde)
        * d/repowerd.service: update bus name (81e84e9)
        * Rename com.canonical.powerd to com.lomiri.Repowerd (8a6500d)
        * Rename com.canonical.repowerd to com.lomiri.Repowerd.Settings
          (d87b9a0)

2021-05-29 Ratchanan Srirattanamet

        * src, tests: rename interfaces to lomiri-system-compositor (f453e89)

2021-05-28 Ratchanan Srirattanamet

        * event_loop: ignore function casting -Werror (ff24112)
        * tests: make sure we don't include gtest_main needlessly (f606f47)
        * CMakeLists: migrate to FindGMock from cmake-extras (36af0bf)

2021-06-10 Dalton Durst

        * Merge branch 'xenial_-_build-without-hybris' into 'ubports/xenial'
          (5b22003)

2021-06-04 Ratchanan Srirattanamet

        * Move Jenkinsfile to debian/ per the new guideline (c4dd9f0)
        * Update Jenkinsfile to use shared library (6b9d4fc)

2021-05-28 Ratchanan Srirattanamet

        * tests/adapter-tests: fix building without hybris/platform-api
          (6a10bc4)
        * default_daemon_config: add missing includes for non-hybris case
          (ea86e35)

2021-03-25 Marius Gripsgard

        * Merge pull request #17 from ubports/xenial_-_wayland-session
          (1375dd3)

2021-03-24 Marius Gripsgard

        * Merge pull request #20 from ubports/xenial_-_clock-alarm (0424d9b)

2021-03-15 Rodney

        * Merge pull request #16 from ubports/xenial_-_missing-header
          (1f5a30e)

2020-05-27 Marius Gripsgard

        * adapters: timerfd: Implement CLOCK_REALTIME_ALARM (1576f3c)

2020-08-14 Marius Gripsgard

        * adapters: logind: Make wayland session a compatable repowerd
          session (5b3b74b)

2021-01-14 Marius Gripsgard

        * Allow to be built without hybris/android/papi support (fc4bc9b)
        * core: Add missing header (025e510)

2020-09-08 Ratchanan Srirattanamet

        * core: do disallow automatic suspend on client request (5c9e029)

2020-08-12 Marius Gripsgard

        * Merge pull request #10 from ubports/xenial_-_fix-proximity-hiccup
          (2a0e480)

2020-07-18 Florian Leeber

        * The Oneplus One also needs to have the initial fake event reversed
          (08419b8)

2020-06-24 Ratchanan Srirattanamet

        * core: actually uses suspend inhibitions in automatic suspend
          decision (946c666)

2020-05-05 Ratchanan Srirattanamet

        * debian: uses a link instead of a file copy for powerd compat config
          (04dce2e)

2020-05-04 Ratchanan Srirattanamet

        * Revert "core: Suspend inhibitions should affect automatic suspend
          only when display turns off due to inactivity" (cfb1cec)
        * Update Jenkinsfile as of ubports/build-tools@10c0987 (955732c)

2020-04-08 Marius Gripsgard

        * Update changelog (36c10ca)

2020-01-22 Florian Leeber

        * Use default config from other directory (#7) (ec4223e)

2018-10-25 Alfred Neumayer

        * adapters: Fix crash in UPowerPowerSource's critical temperature
          assignment Using std::stod when parsing UPower's
          shutdownBatteryTemperature setting causes repowerd to
          crash on Sony Xperia X (arm64 kernel). Use std::stoi
          instead as the setting itself is backed by an integer type
          in UPower itself. (4d3f54c)

2018-01-15 Marius Gripsgard

        * Import to ubp (a6362f4)

2017-12-17 Marius Gripsgard

        * Always set brightness before first autobrightness setting fixes
          https://github.com/ubports/ubuntu-touch/issues/305
          (cd09c43)

2018-01-15 Marius Gripsgard

        * Add Jenkinsfile (ad3225c)

2017-04-03 Bileto Bot

        * Releasing 2017.03+17.04.20170403-0ubuntu1 (9d02adb)

2017-04-03 Alexandros Frantzis

        * 2017.03 release   - Fix "Closing the lid does not suspend the
          device" (LP: #1668596)   - Fix "messages are logged on
          every input event" (LP: #1674736)   - Fix brightness
          issues when turning on the screen on Fairphone 2   - Add
          apport hook (69cb91e)
        * adapters: Fix UnityDisplay::turn_on memory leak (f498d04)

2017-03-29 Alexandros Frantzis

        * Prepare files for 2017.03 release (69d9e6a)

2017-03-28 Alexandros Frantzis

        * debian: Add apport hook (36fabb4)
        * adapters: Ensure dimming a lit display will not increase its
          brightness (56c2aa8)
        * adapters: Fix application of user-set normal brightness value
          (d433783)
        * tests: Make rt::FakeTimer thread-safe to avoid races in tests
          (230ccf5)

2017-03-27 Alexandros Frantzis

        * adapters: Implement system suspend block notifications for
          LogindSystemPowerControl (140ec67)
        * core: Introduce system suspend block infrastructure and core logic
          (e438a1b)
        * core: Suspend inhibitions should affect automatic suspend only when
          display turns off due to inactivity (8fd9f6b)

2017-03-23 Alexandros Frantzis

        * core: Ensure we don't suspend the system when session is inactive
          (b565779)
        * core,adapters: Refactor SystemPowerControl::(dis)allow_suspend()
          methods (87db540)
        * core,adapters: Remove obsolete SystemPowerControl methods (deb4948)
        * core: Implement suspend disallowance logic in DefaultStateMachine
          (2575366)
        * core,adapters: Move suspend dis/allowance handling to the
          per-session state machine (05976cf)
        * adapters: Don't inhibit logind suspends for suspend block requests
          sent to repowerd (7379903)
        * core: Suspend action when closing lid should not respect suspend
          inhibitions (2b15a62)
        * tests: Add test for UnityDisplay::turn_on() maximum wait (2081084)

2017-03-18 Ratchanan Srirattanamet

        * adapters: make the screen-on call to u-s-c synchronous (49761c8)

2017-03-22 Alexandros Frantzis

        * tests: Deduplicate duration_of() helper function (4bda546)
        * debian: Don't log to /var/log/syslog (de8282f)

2017-02-21 Bileto Bot

        * Releasing 2017.02+17.04.20170221-0ubuntu1 (f7263a9)

2017-02-21 Alexandros Frantzis

        * Prepare files for 2017.02 release (83ac5f3)
        * adapters: Generate random cookies for wakeup requests (3510a46)

2017-02-20 Alexandros Frantzis

        * tests: Fix destruction order of test object members (0a94520)

2017-02-17 Alexandros Frantzis

        * adapters: Add missing signal from Unity.Screen introspection
          (926f1ed)
        * doc: Add com.canonical.repowerd DBus API documentation (4be1095)

2017-02-15 Alexandros Frantzis

        * tests: Add REPOWERD_TEST_LOG environment variable (00ce88f)
        * adapters: Improve critical battery behavior (e978612)

2017-02-14 Alexandros Frantzis

        * adapters: Refactor upower based power source to use DisplayDevice
          (db0b761)
        * tools: Add settings support to repowerd-cli (a5b6ab7)

2017-02-13 Alexandros Frantzis

        * adapters: Implement dbus based client settings (4cfb8f7)

2017-02-14 Alexandros Frantzis

        * core: Apply changes to inactivity timeouts when the power source
          changes (b55da16)
        * core: Don't accept the display-off action for the lid behavior
          setting (a2e2693)

2017-02-09 Alexandros Frantzis

        * core: Add core logic for critical power behavior setting (ac81a3a)
        * core: Add critical power behavior support in ClientSettings and
          hook it up in the daemon (22637fd)

2017-02-08 Alexandros Frantzis

        * core: Add core logic for lid behavior settings (188dba8)
        * core: Add lid behavior support in ClientSettings and hook it up in
          the daemon (6a1205f)
        * adapters: Implement logind based system resume notification
          (8a9410d)
        * core: Add system resume handling (d897dfe)

2017-01-31 Alexandros Frantzis

        * core: Add basic inactivity suspend timeout support (756e2fd)
        * tests: Update suspend expectation methods (cf3167f)

2017-01-30 Alexandros Frantzis

        * core,adapters: Add support for battery vs line-power client
          settings (8dcfd6b)

2017-01-27 Alexandros Frantzis

        * core: Introduce ClientSettings (5407b28)

2017-01-26 Alexandros Frantzis

        * core: Introduce StateMachine::handle_set_inactivity_behavior()
          (137b47f)

2017-02-01 Alexandros Frantzis

        * adapters: Implement and use TimerfdWakeupService (ebbff23)

2017-01-17 Alexandros Frantzis

        * core: Improve display handling during lid manipulation (2e9ada7)

2017-01-13 Alexandros Frantzis

        * core,adapters: Update DisplayPowerControl interface to support
          filters (ef55d92)
        * adapters: Rename UnityDisplayPowerControl to UnityDisplay (15346a2)
        * adapters: Implement DisplayConfiguration with
          UnityDisplayPowerControl (0d43e06)

2017-01-12 Alexandros Frantzis

        * core: Add DisplayInformation interface and core logic (433f06d)

2017-01-05 Alexandros Frantzis

        * adapters: Set the names of all event loop threads (956b449)

2017-01-04 Alexandros Frantzis

        * core,adapters: Suspend when allowed with lid closed (8f901e8)

2016-12-12 Bileto Bot

        * Releasing 2016.12+17.04.20161212.1-0ubuntu1 (28c65b0)

2016-12-12 Alexandros Frantzis

        * Prepare files for 2016.12 release (4ed91aa)

2016-12-07 Alexandros Frantzis

        * adapters: Temporarily disallow suspend after a power source change
          (56e524b)
        * adapters: Temporarily inhibit suspend after a hardware alarm is
          triggered (62ea426)
        * adapters: Introduce TemporarySuspendInhibition and implementation
          (2cb7454)
        * adapters: Increase synthetic proximity event delay for mako
          (c879a68)

2016-12-06 Alexandros Frantzis

        * adapters: Use a sensible time value in the future when not needing
          /dev/alarm (537ea5c)

2016-12-07 Alexandros Frantzis

        * adapters: Notify about power source changes involving the "pending
          charge" battery state (638bc74)

2016-12-12 Alexandros Frantzis

        * repowerd: Add REPOWERD_DEVICE_CONFIG_DIR environment variable
          (1c39d81)

2016-12-09 Alexandros Frantzis

        * adapters: Use proper DBus path for dbus manager (91513a1)

2016-12-05 Alexandros Frantzis

        * tests: Fix build with clang (a9c0985)

2016-12-02 Alexandros Frantzis

        * adapters: Inhibit lid handling when disallowing default logind
          system handlers (ef10585)

2016-11-23 Alexandros Frantzis

        * adapters: Rename UPowerPowerSource to UPowerPowerSourceAndLid
          (0640162)
        * adapters: Implement and use upower-based Lid (d93f52e)

2016-12-02 Alexandros Frantzis

        * adapters: Implement LogindSystemPowerControl::suspend_if_allowed()
          (153811b)

2016-11-22 Alexandros Frantzis

        * core: Add lid state machine logic (0686f6f)

2016-11-21 Alexandros Frantzis

        * core: Add Lid interface and hook it up in the daemon (95717ce)

2016-12-02 Alexandros Frantzis

        * core: Fix HandlerRegistration move semantics (c1d81fa)

2016-11-30 Alexandros Frantzis

        * adapters: Log DefaultStateMachine options (0b0a86c)
        * core,adapters: Add treat_power_button_as_user_activity option
          (3af72b2)
        * tests: Refactor ATurnOnDisplayAtStartupOption test (0fda3aa)
        * core,adapters: Move turn_on_display_at_startup option into
          StateMachineOptions (b8a0707)
        * core,adapters: Move state machine timeout options to
          StateMachineOptions (c78dfd6)

2016-12-01 Alexandros Frantzis

        * tests: Fix race in BacklightBrightnessControl tests (4883dca)
        * tests: Fix race in LogindSystemPowerControl tests (88e57ba)

2016-11-30 Alexandros Frantzis

        * adapters: Fix build with g++-4.9 and clang (48d7a81)

2016-11-29 Alexandros Frantzis

        * adapters: Implement
          LogindSystemPowerControl::(dis)allow_default_system_handlers
          (388e916)

2016-11-28 Alexandros Frantzis

        * core: Add SystemPowerControl::(dis)allow_default_system_handlers()
          (77dd443)
        * core: Add StateMachine::start() method (e1f2019)

2016-11-29 Alexandros Frantzis

        * core,adapters: Distinguish different suspend types and deal with
          them accordingly (99f74bc)

2016-11-25 Alexandros Frantzis

        * adapters: Add and use LogindSystemPowerControl (f78f76c)
        * adapters: Add repowerd::Fd move semantics support (0396aa3)
        * core,adapters: Merge ShutdownControl into SystemPowerControl
          (b1447d5)
        * core: Rename SuspendControl to SystemPowerControl (df83d92)

2016-11-29 Alexandros Frantzis

        * core: Pause active session before removing it (0d52e08)

2016-11-24 Bileto Bot

        * Releasing 2016.11+17.04.20161124-0ubuntu1 (9334bde)

2016-11-24 Alexandros Frantzis

        * tests: Add repowerd::Daemon tests for pause and resume (874bf2f)
        * cmake: Update FindGtestGmock.cmake for gtest/gmock 1.8.0
          compatibility (ce97efd)

2016-11-16 Alexandros Frantzis

        * Prepare files for 2016.11 release (5e4cf24)
        * debian: Start by default on desktop systemd-based systems (5658501)
        * adapters: Accept requests without a matching session that originate
          from root or the active session user (3f8dab8)

2016-11-15 Alexandros Frantzis

        * adapters: Add ignore_session_deactivation quirk (bd445ad)

2016-11-10 Alexandros Frantzis

        * core: Support pausing and resuming state machines during session
          switches (0b6b3ec)

2016-11-11 Alexandros Frantzis

        * core: Update the active session when the active session is removed
          (97bb2d3)

2016-11-10 Alexandros Frantzis

        * core: Add override to NullStateMachine (7ab0b57)
        * tests: Use namespace testing globally in Daemon tests (efc4a07)
        * core: Start processing session tracking events before any
          per-session events (051ed29)

2016-11-09 Alexandros Frantzis

        * adapters: Use the logind session id (instead of the logind path)
          for the repowerd session id (1134a79)
        * core: Give each state machine a name and log it (ca1a15c)

2016-11-08 Alexandros Frantzis

        * core,adapters: Dispatch client requests to sessions that own them
          (a2b88ff)
        * core: Move brightness handling into state machine (ed5f6c8)
        * core: Daemon enqueue_action_to_active_session (1fe931c)

2016-11-03 Alexandros Frantzis

        * core,adapters: Introduce and use repowerd::invalid_session_id
          (225ff26)
        * core: Introduce session handling (432ea06)

2016-11-02 Alexandros Frantzis

        * core: Add and use StateMachineFactory (38230c4)
        * tools: Add repowerd-session-tool (48abb04)

2016-11-01 Alexandros Frantzis

        * core: Add DaemonConfig::the_session_tracker() and default
          implementation (805c80d)
        * core,adapters: Add logind-based session tracker (9814d67)

2016-10-27 Alexandros Frantzis

        * core,adapters: Move handling of multiple notifications from
          adapters to core (11fa44f)

2016-10-26 Alexandros Frantzis

        * core: Use a state event adapter instead of changing the state
          machine interface (6d588c6)
        * core,adapters: Move handling of multiple inactivity requests from
          adapters to core (913ca3f)

2016-10-25 Alexandros Frantzis

        * core: Don't expose underlying AlarmId type (04a0019)

2016-10-07 Bileto Bot

        * Releasing 2016.10+16.10.20161007-0ubuntu1 (768279e)

2016-10-07 Alexandros Frantzis

        * 2016.10 release (93f7646)
        * Prepare files for 2016.10 release (55341f9)
        * debian: Sync with released package (dee78d6)

2016-10-06 Alexandros Frantzis

        * debian: Wait for the repowerd DBus API to be accessible before
          declaring the upstart job as started (9096f69)
        * tools: Handle SIGTERM gracefully in repowerd-cli (061d484)
        * tools: Update repowerd-cli help text (fff9535)

2016-09-22 Alexandros Frantzis

        * adapters: Fix typo in brightness config value (1e8d162)

2016-08-30 Alexandros Frantzis

        * Prepare for 2016.08.3 release (b9fa947)
        * core: Enforce an upper limit to the amount of time notifications
          can keep the display on (c259fa7)
        * tools: Support the "active" command in repowerd-cli (450c1d0)

2016-08-24 Alexandros Frantzis

        * Sync with released version (d5020cc)

2016-08-23 Alexandros Frantzis

        * adapters: Log quirks (82d03bd)
        * adapters: Add autobrightness quirk for Nexus 4 (e9fb49a)

2016-08-22 Alexandros Frantzis

        * Prepare for 2016.08.2 release (9e4a848)

2016-08-20 Alexandros Frantzis

        * adapters: Fix autobrightness behavior (LP: #1613871) (051515d)

2016-08-17 Alexandros Frantzis

        * debian: Update changelog for new fix (04120eb)
        * repowerd: Change reduced (AKA short) timeout to 10s (f7cb388)
        * Fix "The call screen still turns off after rejecting the call on
          the remote end without answering." (LP: #1613506)
          (49b0ee4)

2016-08-16 Alexandros Frantzis

        * Prepare for 2016.08.1 release (25ffea5)

2016-08-12 Alexandros Frantzis

        * Don't turn off the screen immediately after a keep on request is
          cancelled and timeouts have expired (6b1f405)

2016-08-11 Alexandros Frantzis

        * Prepare files for 2016.08 release (5224039)

2016-08-09 Alexandros Frantzis

        * Fix suspending during video playback when USB gets unplugged (LP
          #1602933) (011b5f1)

2016-08-08 Alexandros Frantzis

        * debian: Silently fail if lxc-android-config.service is missing
          (33fd6fc)

2016-08-05 Alexandros Frantzis

        * Fix build with g++-6 (b046a41)

2016-07-25 Alexandros Frantzis

        * debian: Update changelog (115d87c)

2016-07-13 Michał Sawicz

        * Wants, not Requires lxc-android-config (LP: #1602597) (452cc65)

2016-07-06 Alexandros Frantzis

        * debian: More packaging improvements (b19a738)
        * debian: Update Vcs-Git field (279f26e)

2016-07-05 Alexandros Frantzis

        * debian: Update changelog with needs-packaging LP bug (dd5494c)
        * debian: Format is 3.0 (native) (3b9966d)

2016-06-13 Alexandros Frantzis

        * adapters: Ignore stray proximity events when the proximity sensor
          is disabled (4a6508d)

2016-06-10 Alexandros Frantzis

        * tests: Change how we split tests into executables (32810a3)
        * Abstract time-related operations and use fake time in tests
          (b0a123d)
        * Abstract filesystem access and remove FUSE test dependency
          (8db2296)
        * tools: Use DefaultDaemonConfig to create components in tools
          (0cf8a8e)

2016-06-09 Alexandros Frantzis

        * debian: Update changelog (72ec03e)
        * repowerd: Log repowerd version (7829fb2)

2016-06-08 Alexandros Frantzis

        * Fix clang build (a3e716a)

2016-06-07 Alexandros Frantzis

        * adapters: Choose sysfs backlight based on type (d5a7dd6)
        * adapters: Log used sysfs backlight path (32dddcb)
        * repowerd: The proximity state of NullProximitySensor is always far
          (ebe9905)
        * adapters: Remove printf messages from mocksuspend backend (7290b72)
        * repowerd: Log fallbacks to null components implementations
          (5067905)
        * repowerd: Allow running without brightness control (7ddb93d)
        * tests: Update filters for tests that need FUSE or are
          time-sensitive (e0ae829)
        * adapters: Fix SysfsBacklight::get_brightness() to return the last
          set brightness value, unless brightness was changed
          externally (e7ca8e6)

2016-06-06 Alexandros Frantzis

        * adapters: Refactor path handling (db1f78a)
        * tests: Support symlinks in VirtualFilesystem (9c6b490)

2016-06-03 Alexandros Frantzis

        * tools: Informational message improvements (ebc3837)
        * Support disabling time-sensitive integration tests (90c2ba5)

2016-06-01 Alexandros Frantzis

        * tests: Include forgotten header (a7565bc)
        * Round brightness calculations for better accuracy (29461c8)
        * Misc fixes (c5daeb6)
        * debian: Explicitly state arch-unqualified package name in powerd
          maintscript to allow proper conf file removal (ad44678)

2016-05-31 Alexandros Frantzis

        * debian: Build depend on libubuntu-platform-hardware-api-dev instead
          of -headers (eda9bfd)
        * adapters: Log android properties (d5c6805)
        * adapters: Properly read device-specific config file (5c23f44)
        * adapters: Add logging to AndroidDeviceConfig (10f7c30)

2016-05-30 Alexandros Frantzis

        * debian: Use an init system neutral postrotation command for
          logrotate (aa9275e)
        * adapters: Transition directly to new brightness value if current
          value is unknown (06ae913)
        * Use double instead of float where possible (4cf5cb9)
        * debian: Make repowerd packages depend on specific versions of other
          repowerd packages (50039b5)
        * tools: Add and ship (re)powerd-cli utility (700f4ae)
        * tests: Keep trying to unmount fuse filesystems on tear down
          (7661853)
        * core: Change disable_inactivity_timeouts to brighten/turn on the
          display (d6d0c08)
        * debian: Remove powerd conf files (a178495)
        * debian: Update package version (a575824)

2016-05-27 Alexandros Frantzis

        * adapters: Implement and use UPower based power source (1bc6ea7)
        * adapters: Implement and use system shutdown control (a370dbf)
        * core: Add handling of critical power source state (8f0d300)

2016-05-26 Alexandros Frantzis

        * core: Add PowerSource and core logic (235a857)
        * repowerd: Properly shutdown on SIGINT and SIGTERM signals (8ade3f7)
        * core: Change internal daemon loop terminology (Event->Action)
          (b32c3ff)
        * repowerd: Read first from powerd device config directory for
          compatibility reasons (3dcbe44)

2016-05-25 Alexandros Frantzis

        * debian: Add logrotate configuration (1b6e9ef)
        * debian: Add rsyslog configuration (1fc7669)
        * data,debian: Install dbus conf files (12c4472)
        * debian: Add repowerd init system startup files (643e567)
        * debian: Update packaging (4cfdcde)
        * build: Install repowerd and tools to sbin (e7f72ad)
        * data: Add device config files (2b486d6)

2016-05-24 Alexandros Frantzis

        * tests: Fix build errors with stricter debian flags (fd252bc)
        * adapters: Log brightness transitions (43e9cb5)
        * adapters: Add a quirk for the type (near/far) of the synthetic
          initial proximity event (86153b7)

2016-05-23 Alexandros Frantzis

        * adapters: Implement Ubuntu platform API based performance booster
          (d0e3c02)
        * core,tests: Add performance booster and logic (8dd2c3e)
        * adapters: Print timestamp of each log entry in ConsoleLog (d2d61dc)

2016-05-20 Alexandros Frantzis

        * adapters: Add libsuspend based suspend control (08eea7e)
        * core,adapters: Add SuspendControl and logic (a43fc34)

2016-05-23 Alexandros Frantzis

        * adapters: Make all public BacklightBrightnessControl methods
          synchronous (a2384d9)

2016-05-19 Alexandros Frantzis

        * adapters: Add logging to OfonoVoiceCallService (36da37f)
        * repowerd: Use the ofono based modem power control by default
          (be2de2d)
        * adapters: Implement ofono based modem power control (64e78c5)

2016-05-18 Alexandros Frantzis

        * tests: Use a variable of the correct type uint64_t when creating a
          GVariant containing a time value (a6ecd8b)
        * core: Add ModemPowerControl and logic (5ca84af)
        * core: Don't store shared_ptr to unused object (6604dec)

2016-05-13 Alexandros Frantzis

        * core,adapters: Add logging (30125fc)

2016-05-18 Alexandros Frantzis

        * core: Initialize forgotten DefaultStateMachine instance variable
          (b95675e)

2016-05-17 Alexandros Frantzis

        * tests: Add some tests for AndroidAutobrightnessAlgorithm (3c1e788)
        * adapters: Improve robustness against crashes of clients emitting
          notifications (49e340a)
        * adapters: Don't emit brightness notification if brightness hasn't
          changed (13284c5)

2016-05-16 Alexandros Frantzis

        * tests: Add tests for AndroidBacklight (712ec75)

2016-05-12 Alexandros Frantzis

        * adapters: Introduce and use an Android libhardware based backlight
          (a332076)
        * repowerd: Fall back to a null light sensor if a real one is not
          available (123a93c)
        * repowerd: Use separate backlight and light sensor creation methods
          in DefaultDaemonConfig (392fef0)
        * adapters: Fix off-by-one error in MonotoneSpline (0b27334)
        * adapters: Implement brightness change notification over DBus
          (eebc906)

2016-05-10 Alexandros Frantzis

        * adapters: Implement autobrightness (736c165)

2016-05-12 Alexandros Frantzis

        * adapters: Use g_variant_iter_next() to avoid memory errors
          (4ec2fa1)

2016-05-11 Alexandros Frantzis

        * debian,build: Build depend on android-headers (73a601c)
        * adapters: Don't allow MonotoneSplines with fewer than two points
          (a1a2869)

2016-05-10 Alexandros Frantzis

        * adapters: Ensure registering an object or signal handlers are
          synchronous (711c6b2)

2016-05-09 Alexandros Frantzis

        * core: Remove ProximitySensor::start_processing() (a6e76c1)
        * adapters,tool: Implement UbuntuLightSensor and related light tool
          (eaee111)
        * tests: Extract TemporaryEnvironmentValue and TemporaryFile classes
          (8079eb8)
        * adapters: Implement monotone spline class (78f856e)
        * Use static libraries with transitive usage requirements instead of
          object libraries (fc78512)

2016-05-05 Alexandros Frantzis

        * adapters: Refactor BacklightBrightnessControl (previously
          SysfsBrightnessControl) to not depend on a specific
          backlight control technology (4dd45d2)

2016-05-04 Alexandros Frantzis

        * adapters: Translate DBus setInactivityTimeouts request infinite
          timeout value to repowerd infinite timeout value (cee3aba)
        * core: Ignore invalid values for inactivity timeouts (0a62fb1)
        * core: Properly handle requests to disable inactivity timeouts
          (4054d80)

2016-04-27 Alexandros Frantzis

        * adapters: Add ofono based voice call service (08c78da)
        * adapters: Powerd DBus API sys state should only affect system
          suspend, not display power (05d722e)
        * tests: Fix race in test expectations (2d4bec1)

2016-04-26 Alexandros Frantzis

        * adapters: Introduce mechanism to unregister dbus object and signal
          handlers (226f492)

2016-04-25 Alexandros Frantzis

        * adapters: Transition smoothly between brightness values (69d24f5)

2016-04-22 Alexandros Frantzis

        * tools: Add wakeup tool (3585617)
        * adapters: Add /dev/alarm based wakeup service (12ecb2e)

2016-04-25 Alexandros Frantzis

        * adapters: Support Powerd DBus API wakeup requests (d6b12db)

2016-04-21 Alexandros Frantzis

        * adapters: Calculate default brightness values based on device
          config (1a25677)
        * adapters: Merge PowerdService into UnityScreenService (c1d6b68)
        * core,adapters: Ensure we start processing requests after all
          handlers have been registered (08e1427)

2016-04-20 Alexandros Frantzis

        * adapters: Add forgotten "#pragma once" (597c96f)
        * core: Small DefaultStateMachine refactoring (ab16485)
        * adapters: Scale brightness values requested by users using the
          device config brightness values (61baad1)
        * adapters: Implement part of the com.canonical.powerd dbus API
          (cc9f99d)

2016-04-15 Alexandros Frantzis

        * core: Improve notification handling (c9a28fa)
        * tests: Fix initialization of fuse_ops struct to support gcc-4.9
          (91943d8)
        * repowerd: Turn on display at startup (5b96d0b)

2016-04-14 Alexandros Frantzis

        * tests: Check for existence of fuse only when building the tests
          (ba9a071)
        * debian: Build depend on 'fuse' package (f848e0f)
        * repowerd: Use production components instead of null implementation
          where possible (8e4d663)
        * adapters: First cut at SysfsBrightnessControl (900f44b)

2016-04-13 Alexandros Frantzis

        * tests: Add virtual filesystem infrastructure (e8a1169)

2016-04-12 Alexandros Frantzis

        * adapters: Implement an event loop based timer (e986d86)
        * core: Add Timer::cancel_alarm() (52cbb0b)

2016-04-11 Alexandros Frantzis

        * adapters: Implement DBus based user activity (f35cb84)
        * adapters: Implement a DBus based power button event sink (b7b93bd)
        * adapters: Implement DBus based power button (e8fc886)
        * adapters: Fix UnityDisplayPowerControl header (66cd5b7)

2016-04-08 Alexandros Frantzis

        * adapters: Implement DBus based display power control (2e84337)
        * adapters: Improve UbuntuProximitySensor concurrency correctness
          (e1cddb2)
        * adapters: Introduce device quirks support (77043ff)

2016-04-07 Alexandros Frantzis

        * tools: Add proximity tool (d92851a)
        * Update debian/control build-deps (09d59dd)
        * adapters: Implement proximity sensor based on
          ubuntu-application-api (993536b)

2016-04-05 Alexandros Frantzis

        * adapters: Implement display power event notification in
          UnityScreenService (bd33a07)

2016-04-04 Alexandros Frantzis

        * core: Implement display power event notification logic (19c8157)

2016-04-01 Alexandros Frantzis

        * adapters: Properly register and unregister handlers in
          UnityScreenService (ff7092e)
        * adapters: Add notification support to UnityScreenService (044b05f)

2016-03-31 Alexandros Frantzis

        * tests,adapters: First cut at DBus infrastructure and
          UnityScreenService (74ec4ff)

2016-03-29 Alexandros Frantzis

        * core: Move core code into core/ directory (26f4ad6)
        * tests: Fix race in acceptance tests (57eba3e)
        * daemon,tests: Implement 'set_inactivity_timeout' client request
          (81c11e8)

2016-03-28 Alexandros Frantzis

        * daemon,tests: Implement 'enable/disable_autobrightness' client
          requests (3b29dcc)
        * daemon,tests: Implement 'set_normal_brightness_value' client
          request (0e9e52a)

2016-03-23 Alexandros Frantzis

        * daemon,tests: Support voice call use cases (c8294a6)

2016-03-24 Alexandros Frantzis

        * tests: Deduplicate timeout variables (793f95c)

2016-03-23 Alexandros Frantzis

        * daemon,tests: Support more notification use cases (c390c3e)

2016-03-21 Alexandros Frantzis

        * daemon,tests: Support inactivity timeout dispaly dimming (7bb1bc0)

2016-03-18 Alexandros Frantzis

        * daemon,tests: Support notification use cases (2a0eb40)

2016-03-17 Alexandros Frantzis

        * daemon,tests: Support "enable/disable_inactivity_timeout" client
          request and remove "turn_on_display" request (dbf4738)
        * Refactor handler registration and unregistration (8489673)

2016-03-11 Alexandros Frantzis

        * daemon,tests: Support "turn_on_display" client request (29cb01c)

2016-03-10 Alexandros Frantzis

        * daemon,tests: Fix power button long press behavior (8e60c78)
        * daemon,tests: Support proximity use cases (d300ca0)

2016-03-09 Alexandros Frantzis

        * tests: Improve test names (964f422)
        * daemon,tests: Implement user activity use cases (a742b9f)
        * Introduce DaemonConfig methods for timeouts (a9d3a28)
        * tests: Deduplicate common acceptance test code (e1755f0)
        * daemon,tests: Added basic user inactivity display blanking logic
          (108011d)

2016-03-08 Alexandros Frantzis

        * Be consistent about using "power button" vs "power key" (648ddb4)
        * tests: Add unit tests for repowerd::Daemon (e6c730c)
        * tests: Add power button long press acceptance tests (1f4c4b6)

2016-03-03 Alexandros Frantzis

        * More power button tests and logic (422ad61)
        * Remove redundant curly braces (09f2e18)

2016-03-02 Alexandros Frantzis

        * Add a simple event loop and StateMachine abstraction (dd45f05)
        * Walking skeleton (1ad0bdb)
